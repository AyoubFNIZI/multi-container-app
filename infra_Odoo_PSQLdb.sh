#!/bin/bash

# Define the number of times to execute the playbook

echo "Executing the first playbook"
ansible-playbook playbook_cshell_creaVM.yml

sleep 3


echo "Executing the second playbook"
ansible-playbook playbook_cshell_creaContainers.yml
